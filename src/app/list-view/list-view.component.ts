import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Services
import { ColorsService } from '../services/colors.service';
import { UtilitiesService } from '../services/utilities.service';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit {
  private total_colors_per_page = 8;
  private originals_colors_object;
  public colors_list;
  public export_colors_list;

  // Pagination
  public total_color_pages;
  public current_color_page = 0;
  public color_begin = 0;
  public color_end = this.total_colors_per_page;

  constructor(
    private colors_service: ColorsService,
    private utilities: UtilitiesService,
    private router: Router
  ) { }

  private createDisplayColorObject(colors) {
    this.export_colors_list = colors;
    this.colors_list = this.mapArray(colors);
    this.paginationArray(this.colors_list);
  }

  private filterColors(filter_text) {
    if (!this.originals_colors_object) { return; }
    const filtered_colors = this.originals_colors_object.filter(color => color.color_id.startsWith(filter_text));
    this.createDisplayColorObject(filtered_colors);
    this.paginateTo(0);
  }

  private mapArray(colors) {
    const total_colors = [];
    for (const color of colors) {
      for (const shade of color.shades) {
        const new_color_object = {
          color_id: color.color_id,
          shade: shade
        };
        total_colors.push(new_color_object);
      }
    }
    return total_colors;
  }

  private paginationArray(all_colors) {
    this.total_color_pages = Array(Math.ceil(all_colors.length / this.total_colors_per_page));
  }

  public exportColors() {
    this.utilities.exportColors(this.export_colors_list);
  }

  public paginateTo(pageNumber) {
    this.current_color_page = pageNumber;
    this.color_begin = pageNumber * this.total_colors_per_page;
    this.color_end = this.color_begin + this.total_colors_per_page;
  }

  public selectedColor(color) {
    this.colors_service.setSelectedColor(color);
    this.router.navigate(['/detailview']);
  }

  ngOnInit() {
    this.colors_service.getColors().subscribe(colors => {
      this.originals_colors_object = colors;
      this.createDisplayColorObject(colors);
    });

    this.colors_service.filter_text.subscribe(text => {
      this.filterColors(text);
    });
  }

}
