import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

// Environments
import { environment } from '../environments/environment';

// Components
import { AppComponent } from './app.component';
import { ListViewComponent } from './list-view/list-view.component';
import { DetailViewComponent } from './detail-view/detail-view.component';

// Services
import { ColorsService } from './services/colors.service';
import { UtilitiesService } from './services/utilities.service';

export const routes: Routes = [
  { path: '', redirectTo: 'listview', pathMatch: 'full' },
  { path: 'listview', component: ListViewComponent },
  { path: 'detailview', component: DetailViewComponent },
  { path: '**', redirectTo: 'listview', pathMatch: 'full' }
];


@NgModule({
  declarations: [
    AppComponent,
    ListViewComponent,
    DetailViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload'
    })
  ],
  providers: [ColorsService, UtilitiesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
