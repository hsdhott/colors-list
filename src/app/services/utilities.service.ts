import { Injectable } from '@angular/core';

@Injectable()
export class UtilitiesService {

  constructor() { }

  public camelCaseFilterText(text) {
    return text.split('').map(function(word, index) {
      // If it is the first word make sure to lowercase all the chars.
      if (index !== 0) {
        return word.toLowerCase();
      }
      // If it is not the first word only upper case the first char and lowercase the rest.
      return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
    }).join('');
  }

  public exportColors(export_colors_list) {
    const myjson = JSON.stringify(export_colors_list, null, 2);
    const x = window.open();
    x.document.open();
    x.document.write('<html><body><pre>' + myjson + '</pre></body></html>');
    x.document.close();
  }
}
