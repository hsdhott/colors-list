import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class ColorsService {
  private selected_color_object;
  private new_filter_text = new Subject<string>();

  public colors;
  public filter_text = this.new_filter_text.asObservable();

  constructor(private database: AngularFireDatabase) { }

  private retrieveColors() {
    return this.database.list('/colors').valueChanges();
  }

  public getFilterText(text: string) {
    this.new_filter_text.next(text);
  }
  public getColors() {
    if (!this.colors) {
      this.colors = this.retrieveColors();
    }
    return this.colors;
  }

  public setSelectedColor(color_object) {
    this.selected_color_object = color_object;
  }

  public getSelectedColor() {
    return this.selected_color_object;
  }
}
