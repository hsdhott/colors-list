import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Services
import { ColorsService } from '../services/colors.service';
import { UtilitiesService } from '../services/utilities.service';

@Component({
  selector: 'app-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class DetailViewComponent implements OnInit {
  private all_colors;
  private complete_color_shade_object;

  public main_color;
  public color_shades;

  constructor(
    private colors_service: ColorsService,
    private utilities: UtilitiesService,
    private route: Router
  ) { }

  private createMainColor(color_id, shade) {
    this.main_color = {
      color_id: color_id,
      shade: shade
    };
  }

  private getShadedColors (color_object) {
    if (color_object.shades instanceof Array) {
      this.createMainColor(color_object.color_id, color_object.shades[0]);
      this.complete_color_shade_object = color_object;
      return color_object.shades;
    }

    for (const color of this.all_colors) {
      if (color.color_id === color_object.color_id) {
        this.complete_color_shade_object = color;
        return color.shades;
      }
    }
  }

  public exportColors() {
    this.utilities.exportColors(this.complete_color_shade_object);
  }

  public updateMainColor(shade) {
    this.createMainColor(this.main_color.color_id, shade);
  }

  ngOnInit() {
    this.colors_service.getColors().subscribe(colors => {
      this.all_colors = colors;
      const selected_color = this.colors_service.getSelectedColor();
      if (!selected_color) {
        this.route.navigate(['/listview']);
        return;
      }
      this.main_color = selected_color;
      this.color_shades = this.getShadedColors(this.main_color);
    });

    // console.log(this.route);
    this.route.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };
  }

}
