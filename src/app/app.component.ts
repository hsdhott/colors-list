import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

// Services
import { ColorsService } from './services/colors.service';
import { UtilitiesService } from './services/utilities.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  public colors;
  public filtered_colors;
  public filter_input = false;
  public filter_text =  '';

  constructor(
    private colors_service: ColorsService,
    private router: Router,
    private utilities_service: UtilitiesService
  ) {
    localStorage.removeItem('firebase:previous_websocket_failure');
  }

  public applyFilter(text) {
    const camel_case_text = this.utilities_service.camelCaseFilterText(text);
    this.colors_service.getFilterText(camel_case_text);
    this.filtered_colors = this.colors.filter(color => color.color_id.startsWith(camel_case_text));
  }

  public clearFilter(filter) {
    this.filter_text = '';
    this.applyFilter(this.filter_text);
  }

  public chooseRandomColor() {
    if (!this.colors) { return; }

    const colorIndex = Math.floor((Math.random() * this.colors.length));
    const chosen_color = this.colors[colorIndex];
    const shadeIndex = Math.floor((Math.random() * chosen_color.shades.length));
    const chosen_color_object = {
      color_id: chosen_color.color_id,
      shade: chosen_color.shades[shadeIndex]
    };
    this.selectedColor(chosen_color_object);
  }

  public selectedColor(color) {
    this.colors_service.setSelectedColor(color);
    this.router.navigate(['/detailview']);
  }

  ngOnInit() {
    this.colors_service.getColors().subscribe(colors => {
      this.colors = this.filtered_colors = colors;
    });
  }
}
